<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Formulario de Ejemplar</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>

<form action="create-ejemplar.php" method="post">
<table>
  <caption>Información del ejemplar</caption>
  <tbody>
    <tr>
      <th>clave</th>
      <td><input type="text" name="clave" /></td>
    </tr>
    <tr>
      <th>conservacion</th>
      <td><textarea name="conserva"></textarea></td>
    </tr>
    <tr>
      <th>ISBN</th>
      <td><input type="text" name="isbn"></textarea></td>
    </tr>
  </tbody>
</table>
<input type="submit" name="submit" value="CREATE" />
</form>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
</ul>

</body>
</html>