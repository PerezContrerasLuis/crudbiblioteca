<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Información de ejemplar</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>
<?php
  $isbn = $_GET['isbn'];

  if (empty($isbn)) {
?>
  <p>Error, no se ha indicado el ISBN del libro</p>
<?php
  } else {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select clave_ejemplar, conservacion_ejemplar, isbn
      from biblioteca.ejemplar
      where isbn = '".$isbn."';";

    $ejemplar = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($ejemplar) == 0) {
?>
  <p>No se ha encontrado algún libro con ISBN<?php echo $isbn ?></p>
<?php
    } else {
      $tupla = pg_fetch_array($ejemplar, null, PGSQL_ASSOC);
	 
      $conser = $tupla['conservacion_ejemplar'];
	  $isbn2 = $tupla['isbn'];
?>
<table>
  <caption>Información del Ejemplar</caption>
  <tbody>
   
    <tr>
      <th>Conservacion</th>
      <td><?php echo $conser; ?></td>
    </tr>
    <tr>
      <th>ISBN</th>
      <td><?php echo $isbn2; ?></td>
    </tr>
    <tr>
      <th>Autor/es</th>
      <td>
<?php
      $query = "select nombre_autor
        from biblioteca.libro_autor as LA
        inner join biblioteca.autor as A
          on (LA.id_autor = A.id_autor and LA.isbn = '".$isbn."');";

      $ejemplar = pg_query($query) or die('La consulta falló: ' . pg_last_error());
      if (pg_num_rows($ejemplar) == 0) {
?>
        <p>Sin autor</p>
<?php
      } else {
?>
        <ul>
<?php
        while ($tupla = pg_fetch_array($ejemplar, null, PGSQL_ASSOC)) {
          foreach ($tupla as $atributo) {
?>
          <li><?php echo $atributo; ?></li> 
<?php
          }
        }
?>
        </ul>
<?php
      }
?>
    </tr>
    <tr>
      <th>Libros</th>
      <td>
<?php
      $query = "select titulo_libro
        from biblioteca.libro as L
        inner join biblioteca.ejemplar as E
          on (L.isbn = E.isbn and E.isbn = '".$isbn."');";
      
      $libros = pg_query($query) or die('La consulta falló: ' . pg_last_error());
      if (pg_num_rows($libros) == 0) {
?>
        <p>Sin libros</p>
<?php
      } else {
?>
        <ul>
<?php
        while ($tupla = pg_fetch_array($libros, null, PGSQL_ASSOC)) {
          $titulo = $tupla['titulo_libro'];
          
?>
          <li><?php echo $titulo; ?></li> 
<?php
        }
?>
        </ul>
<?php
      }
    }
  }
?>
    </tr>
  </tbody>
</table>

<?php
  pg_free_result($result);
  pg_close($dbconn);
?>

  
<form action="delete-ejemplar.php" method="post">
  <input type="hidden" name="isbn" value="<?php echo $isbn; ?>" />
  <p>¿Está seguro/a de eliminar este Ejemplar?</p>
  <input type="submit" name="submit" value="DELETE" />
</form>

<p>  ==============================  </p>

<form action="ejemplares.php" method="post">
  <input type="submit" name="submit" value="Cancelar" />
</form>


<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="ejemplares.php">Lista de ejemplares</a></li>
</ul>

</body>
</html>