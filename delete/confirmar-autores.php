<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Información del Autor</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>
<?php
  $idautor = $_GET['id_autor'];

  if (empty($idautor)) {
?>
  <p>Error, no se ha indicado el ID del libro</p>
<?php
  } else {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select id_autor, nombre_autor
      from biblioteca.autor
      where id_autor = '".$idautor."';";

    $autor = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($autor) == 0) {
?>
  <p>No se ha encontrado algún libro con ID <?php echo $idautor; ?></p>
<?php
    } else {
      $tupla = pg_fetch_array($autor, null, PGSQL_ASSOC);
      $nombre = $tupla['nombre_autor'];
?>
<table>
  <caption>Información del Autor</caption>
  <tbody>
    <tr>
      <th>ID</th>
      <td><?php echo $idautor; ?></td>
    </tr>
    <tr>
      <th>Nombre</th>
      <td><?php echo $nombre; ?></td>
    </tr>
    <tr>
      <th>libros</th>
      <td>
<?php
      $query = "select titulo_libro
        from biblioteca.libro_autor as LA
        inner join biblioteca.libro as L
          on (LA.isbn = L.isbn and LA.id_autor= '".$idautor."');";

      $autores = pg_query($query) or die('La consulta falló: ' . pg_last_error());
      if (pg_num_rows($autores) == 0) {
?>
        <p>Sin libros</p>
<?php
      } else {
?>
        <ul>
<?php
        while ($tupla = pg_fetch_array($autores, null, PGSQL_ASSOC)) {
          foreach ($tupla as $atributo) {
?>
          <li><?php echo $atributo; ?></li> 
<?php
          }
        }
?>
        </ul>
<?php
      }
?>
    </tr>
    <tr>
      <th>Ejemplar/es</th>
      <td>
<?php
      $query = "select clave_ejemplar, conservacion_ejemplar
        from biblioteca.ejemplar as E inner join 
         biblioteca.libro as L on E.isbn= L.isbn
         inner join biblioteca.libro_autor as LA on L.isbn=LA.isbn
         inner join biblioteca.autor as A on LA.id_autor=A.id_autor
        where A.id_autor = '".$idautor."';";

      $ejemplares = pg_query($query) or die('La consulta falló: ' . pg_last_error());
      if (pg_num_rows($ejemplares) == 0) {
?>
        <p>Sin ejemplares</p>
<?php
      } else {
?>
        <ul>
<?php
        while ($tupla = pg_fetch_array($ejemplares, null, PGSQL_ASSOC)) {
          $clave_ejemplar = $tupla['clave_ejemplar'];
          $conservacion_ejemplar = $tupla['conservacion_ejemplar'];
?>
          <li><?php echo $clave_ejemplar." / ".$conservacion_ejemplar; ?></li> 
<?php
        }
?>
        </ul>
<?php
      }
    }
  }
?>
    </tr>
  </tbody>
</table>

<?php
  pg_free_result($result);
  pg_close($dbconn);
?>

<form action="delete-autor.php" method="post">
  <input type="hidden" name="id_autor" value="<?php echo $idautor; ?>" />
  <p>¿Está seguro/a de eliminar este Autor?</p>
  <input type="submit" name="submit" value="DELETE" />
</form>

<p>  ==============================  </p>

<form action="autores.php" method="post">
  <input type="submit" name="submit" value="Cancelar" />
</form>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="autores.php">Lista de Autores</a></li>
</ul>

</body>
</html>