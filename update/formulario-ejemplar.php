<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Formulario de Ejemplar</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>



<?php
  $isbn = $_GET['isbn'];
  $clave = $_GET['clave_ejemplar'];
  $con = $_GET['conservacion_ejemplar'];
  

  if (empty($isbn)) {
?>
  <p>Error, no se ha indicado el ISBN del libro</p>
<?php
  } else {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());
    
	$query = "select clave_ejemplar, conservacion_ejemplar, isbn from biblioteca.ejemplar  where isbn = '".$isbn."';";

    $libro = pg_query($query) or die('La consulta falló: ' . pg_last_error());
  }

    if (pg_num_rows($libro) == 0) {
?>
  <p>No se ha encontrado algún libro con ISBN <?php echo $isbn; ?></p>
<?php
    } else {
      $tupla = pg_fetch_array($libro, null, PGSQL_ASSOC);
      $clave = $tupla['clave_ejemplar'];
	  $con = $tupla['conservacion_ejemplar'];
	}
?>

	
	

<form action="update-ejemplar.php" method="post">
<table>
  <caption>Información del ejemplar</caption>
  <tbody>
  	
  	 <tr>
      <th>ISBN</th>
      <td><input type="text" name="isbn" value="<?php echo $isbn; ?>"></imput></td>
    </tr>
    <!--
    <tr>
      <th>clave</th>
      <td><input type="text" name="clave" value="<?php echo $clave; ?>"/></td>
    </tr>
    <tr> -->
      <th>conservacion</th>
      <td><textarea name="conservacion_ejemplar" value="<?php echo $con; ?>"></textarea></td>
    </tr>
   
  </tbody>
</table>
<input type="submit" name="submit" value="UPDATE" />
</form>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
</ul>

</body>
</html>